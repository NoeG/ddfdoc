Visualisation de données issues du DDF
======================================


# Localisation de définitions

<iframe title="Nombre de d&eacute;finitions localis&eacute;es par localisation r&eacute;f&eacute;renc&eacute;es " aria-label="World Symbol map" id="datawrapper-chart-vqTQH" src="//datawrapper.dwcdn.net/vqTQH/1/" scrolling="no" frameborder="0" style="width: 0; min-width: 100% !important; border: none;" height="400"></iframe><script type="text/javascript">!function(){"use strict";window.addEventListener("message",function(a){if(void 0!==a.data["datawrapper-height"])for(var e in a.data["datawrapper-height"]){var t=document.getElementById("datawrapper-chart-"+e)||document.querySelector("iframe[src*='"+e+"']");t&&(t.style.height=a.data["datawrapper-height"][e]+"px")}})}();</script>