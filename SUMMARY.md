# Summary

* [Introduction](README.md)
* [Présentation du projet](presentation-du-projet.md)
* [Mentions légales](mentions-legales.md)
* [Aide et documentation](aide/README.md)
    * [Recherche et compte utilisateur](aide/recherche-et-compte-utilisateur.md)
    * [Comprendre un article de dictionnaire](aide/comprendre-un-article-du-dictionnaire.md)
* [Visualisation de données](dataviz.md)
* [Partenaires du projet](partenaires.md)