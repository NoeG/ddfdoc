Édité par :

![MC](https://mnemotix.gitlab.io/ddfdoc/img/MIN_Culture_RVB.png)


Maîtrise d’ouvrage et expertise scientifique :

![Lyon 3](https://mnemotix.gitlab.io/ddfdoc/img/lyon3.png)
![2IF](https://mnemotix.gitlab.io/ddfdoc/img/Kx300_Logo_2iF_VECTO.jpg)


Hébergement :

![CNRS](https://mnemotix.gitlab.io/ddfdoc/img/CNRS.png)
![Huma-Num](https://mnemotix.gitlab.io/ddfdoc/img/huma-num.jpg)


Partenaires :

![OIF](https://mnemotix.gitlab.io/ddfdoc/img/OIF.jpg)
![AUF](https://mnemotix.gitlab.io/ddfdoc/img/Logo_AUF.png)
![TV5Monde](https://mnemotix.gitlab.io/ddfdoc/img/tv5monde.png)
![Medias](https://mnemotix.gitlab.io/ddfdoc/img/logo_france_medias_monde.png)


Ils contribuent au contenu :

![Wiktionnaire](https://mnemotix.gitlab.io/ddfdoc/img/WiktionaryFr.svg.png)
![UCLouvain](https://mnemotix.gitlab.io/ddfdoc/img/1024px-UCLouvain_Saint-Louis_-_Bruxelles.png)
![ULaval](https://mnemotix.gitlab.io/ddfdoc/img/Université_Laval.png)
![OQLF](https://mnemotix.gitlab.io/ddfdoc/img/oqlf.jpg)
![ATILF](https://mnemotix.gitlab.io/ddfdoc/img/atilf.jpg)
![ASOM](https://mnemotix.gitlab.io/ddfdoc/img/asom.jpg)


Développement du socle technique :

![Mnemotix](https://mnemotix.gitlab.io/ddfdoc/img/mnemotix.jpg)
